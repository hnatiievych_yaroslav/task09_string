package com.hnatiievych.controller;

import java.util.List;

public interface Controller {
    public boolean checksSentence (String str);
    public List<String> splitsentenceofWord (String str);
    public String replaceVowels (String str);

}
