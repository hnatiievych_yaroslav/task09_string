package com.hnatiievych.controller;

import com.hnatiievych.model.Model;

import java.util.List;

public class ControllerImpl implements Controller {

    @Override
    public boolean checksSentence(String str) {
        return Model.checksSentence(str);
    }

    @Override
    public List<String> splitsentenceofWord(String str) {
        return Model.splitsentenceofWord(str);
    }

    @Override
    public String replaceVowels(String str) {
        return Model.replaceVowels(str);
    }
}
