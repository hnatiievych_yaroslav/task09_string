package com.hnatiievych.model;

import com.fasterxml.jackson.databind.deser.DataFormatReaders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public interface Model {
    public static boolean checksSentence (String str){
        Pattern pattern = Pattern.compile("^[A-Z].+\\.");
        Matcher matcher = pattern.matcher(str);
        boolean result = matcher.matches();
        return result;
    }


    public static List<String> splitsentenceofWord (String str){
        List <String> sentence = new ArrayList<>(Arrays.asList(str.split("the|you")));
        return sentence;
    }

    public static String replaceVowels (String str) {
        String pattern = "[aeiouAEIOU]";
        String result = str.replaceAll(pattern, "_");
        return result;
    }
}
