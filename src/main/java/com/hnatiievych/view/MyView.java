package com.hnatiievych.view;

import com.hnatiievych.controller.Controller;
import com.hnatiievych.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class MyView {

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner scan = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(MyView.class);
    private Controller controller;
    private Locale locale;
    private ResourceBundle bundle;

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("7", bundle.getString("7"));
        menu.put("8", bundle.getString("8"));
        menu.put("Q", bundle.getString("Q"));
    }

    public MyView() {
        controller = new ControllerImpl();
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::testStringUtils);
        methodsMenu.put("2", this::internationalizeMenuUkraine);
        methodsMenu.put("3", this::internationalizeMenuEnglish);
        methodsMenu.put("4", this::internationalizeMenuChina);
        methodsMenu.put("5", this::internationalizeMenuSpain);
        methodsMenu.put("6", this::testCheckString);
        methodsMenu.put("7", this::testSplitString);
        methodsMenu.put("8", this::testreplaceVowels);
    }

    private void testCheckString() {
        Scanner scanner = new Scanner(System.in);
        logger.info("White a sentence");
        logger.info(controller.checksSentence(scanner.nextLine()));
    }

    private void testSplitString() {
        logger.info("Write a sentence");
        logger.info(controller.splitsentenceofWord(scan.nextLine()));
    }

    private void testreplaceVowels() {
        logger.info("write a sentence");
        logger.info(controller.replaceVowels(scan.nextLine()));
    }



    private void testStringUtils() {
        System.out.println("test String utils");
    }

    private void internationalizeMenuUkraine() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuChina() {
        locale = new Locale("zh");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuSpain() {
        locale = new Locale("es");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void testRegEx() {
    }


    private void test() {
    }


    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = scan.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
